<?
	require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
	$APPLICATION->SetTitle('Жюри');
	CJSCore::Init(array('window'));
?>

<?
	//скрипт всплывающего окна
	$login = $_POST['REGISTER']['LOGIN'];
	$email = $_POST['REGISTER']['EMAIL'];
	
	$dialog = array(
		'title'        => 'Регистрация',
		'content_url'  => 'form.php',
		'content_post' => "login=$login&email=$email",
		'draggable'    => true,
		'resizable'    => false,
		'height'       => '200',
		'width'        => '300'
	);
	$strParams = CUtil::PhpToJsObject($dialog);
	$strParams = str_replace('\'[code]', '', $strParams);
	$strParams = str_replace('[code]\'', '', $strParams);
	//ссылка для открытия окна
	$url = 'javascript:(new BX.CDialog('.$strParams.')).Show()';
?>

<?
	//Компонент для вывода Жюри 
	$APPLICATION->IncludeComponent(
		'bitrix:news.list', 
		'template1',
		array(
			'IBLOCK_TYPE' => 'jury', //Тип инфоблока
			'IBLOCK_ID'   => '10', //Идентификатор информационного блока
			'NEWS_COUNT'  => '' //Количество новостей на одной странице
		)
	);
?>

<?//popup с формой Регистрации?>
<div class='div-button text-center'><button type='button' class='button' onclick="<?=$url?>">Подать заявку</button></div>

<div class='reg text-center' id='reg'>Регистрация</div>
<?//Форма Регистрации?>
<?require($_SERVER['DOCUMENT_ROOT'].'/landing/form.php');?>

<? 
	require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php'); 
?>