<?
	include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
?>

<?
	//Форма регистрации
	$APPLICATION->IncludeComponent(
		'bitrix:main.register',
		'template_registration',
		array(
			'AUTH'         => 'Y', //Авторизация после регистрации.
			'SET_TITLE'    => 'N', //Не устанавливать заголовок.
			'SUCCESS_PAGE' => '/landing/index.php?reg=1', //переход после регистрации
		)
	); 
?>