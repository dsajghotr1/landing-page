<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="news-list container-fluid">
	<div class="row justify-content-around">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?	
				//Редактирование и удаление элементов
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), 
					array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<? //Вывод одного элемента на экран ?>
			<div class="p-4 col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12" id="<?=$this->GetEditAreaId($arItem['ID']);?>">	
				<? //Вывод картинки из preview ?>
				<?if($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])):?>
					<img class="preview_picture" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
				<?else:?>
					<img class="preview_picture" src="/images/nofoto.jpg">
				<?endif?>
				<div class="text">
					<? //Названия элемента ?>
					<?if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]):?>
						<b><?= $arItem["NAME"]?></b>
						<br>
					<?endif;?>
					<? //Текст из preview ?>
					<?if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]):?>
						<?= $arItem["PREVIEW_TEXT"];?>
					<?endif;?>
				</div>
			</div>
		<?endforeach;?>
	</div>
</div>