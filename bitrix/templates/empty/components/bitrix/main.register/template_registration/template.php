<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */
use Bitrix\Main\Mail\Event;
?>

<div class="container-fluid">
	<?// Если пользователь авторизован ?>
	<?if($USER->IsAuthorized()):?>
		<div class="row">
			<div class="autor col text-center p-3">
				<?echo GetMessage("MAIN_REGISTER_AUTH")?>	
			</div>
		</div>
	<?else:?>
	<?// Если пользователь не авторизован ?>

		<form method="post" action="index.php#reg">
			<? //Поля для заполнения формы ?>
			<div class="row">
				<div class="col text-center p-1">
				<?// Проходимся по всем Параметрам и выводим их ?>
					<?$passowrd = randString(6);?>
					<?foreach ($arResult["SHOW_FIELDS"] as $FIELD):?>
						<?if ($FIELD === "LOGIN"):?>
							<?
								if (isset($_POST['login'])) 
								{
									$arResult['VALUES']['LOGIN'] = $_POST['login'];
								}
							?>
							<input size="30" type="text" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" 
								autocomplete="off" placeholder="Введите имя" class="input" required pattern="[А-Яа-яЁё]{3,15}">
						<?endif;?>

						<?if ($FIELD === "EMAIL"):?>
							<?
								if (isset($_POST['email'])) 
								{
									$arResult['VALUES']['EMAIL'] = $_POST['email'];
								}
							?>
							<input size="30" type="email" name="REGISTER[<?=$FIELD?>]" value="<?=$arResult["VALUES"][$FIELD]?>" 
								autocomplete="off" placeholder="E-Mail" class="input" required>
						<?endif;?>
						<?if ($FIELD === "PASSWORD"):?>
							<input size="30" type="hidden" name="REGISTER[<?=$FIELD?>]" autocomplete="off" value="<?=$passowrd?>">
						<?endif;?>
						<?if ($FIELD === "CONFIRM_PASSWORD"):?>
							<input size="30" type="hidden" name="REGISTER[<?=$FIELD?>]" autocomplete="off" value="<?=$passowrd?>">
						<?endif;?>
					<?endforeach?>
				</div>
			</div>
			<? //Кнопка Регистрации ?>
			<div class="row">
				<div class="col p-2 text-center">
					<input class="input_reg" type="submit" name="register_submit_button" value="Подтвердить E-Mail">
				</div>
			</div>
		</form>

		<? //Ошибки ?>
		<?if (count($arResult["ERRORS"]) > 0):
			foreach ($arResult["ERRORS"] as $key => $error) 
			{
				$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
			}?>
			<?if (stripos($error, 'Пользователь с таким e-mail') !== false):?>
				<?//Вытаскиваем user по Email
				$email = $_POST['REGISTER']['EMAIL'];
				$filter = Array(
					'EMAIL' => "$email",
				);
				$rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
				$arUser = $rsUsers->Fetch();
				//Проверяем пользователя на атививность
				if ($arUser[ACTIVE] === "N"):
					$id = $arUser[ID];
					//Делаем новый код для активации пользователя
					$code = randString(8);
					$fields = array(
						'CONFIRM_CODE' => "$code",
					);
					//Меняем код активации у пользователя
					$USER->Update($id, $fields);
					//Отсылаем сообщение
					Event::send(
						array(
						    "EVENT_NAME" => "NEW_USER_CONFIRM",
						    "LID"        => "s1",
						    "C_FIELDS"   => array(
						        "EMAIL"        => "$email",
						        "USER_ID"      => "$id",
						        "CONFIRM_CODE" => "$code",
						    ),
						)
					);?>
					<script type='text/javascript'>
						location.href = '/landing/index.php?reg_confirm=1';
					</script>
				<?else:?>
					<div class="row">
						<div class="col text-center error p-3">
							Пользователь с данным E-mail уже создан.
						</div>
					</div>
				<?endif;?>
			<?elseif (stripos($error, 'Пользователь с логином') !== false):?>
				<div class="row">
					<div class="col text-center error p-3">
						Пользователь с данным Именем уже создан.
					</div>
				</div>
			<?else:?>
				<div class="row">
					<div class="col text-center error p-3">
						E-mail не корректен.
					</div>
				</div>
			<?endif;?>
		<?endif;?>

		<? //Сообщения о регистрации ?>
		<?if(isset($_GET['reg'])):?>
			<div class="row">
				<div class="col text-center noerror p-3">
					Спасибо за регистрацию, Вам отправлено письмо с подтверждением!
				</div>
			</div>
		<?endif;?>
		<?if(isset($_GET['reg_confirm'])):?>
			<div class="row">
				<div class="col text-center noerror p-3">
					На данную почту отправлено повторное сообщение активации.
				</div>
			</div>
		<?endif;?>
	<?endif;?>
</div>